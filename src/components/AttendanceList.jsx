import React, {Component} from 'react';
import { Row, Col, Table } from 'react-bootstrap'
import { Style } from 'radium'
import { Link } from 'react-router'
import moment from 'moment'
import { List } from 'immutable'

export default class AttendanceList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sel_items: List(),
    }
  }
  componentWillMount() {
      
  }
  checkBoxChange(item, seq) {
    let cnt = this.state.sel_items
    
            
        

    if (document.getElementById(item.MEALDATE + seq + 'checkbox').checked == false) {
      console.log(item);
      this.setState(prevState => ({
        sel_items: prevState.sel_items.filter(el => el !== item )
        }));
    }
    else {
        this.setState(prevState => ({
            sel_items: prevState.sel_items.push(item)
            }));
    }
    console.log(cnt);
    
  }
  addToparams(itemName){
    console.log(itemName)
  }
  render() {
    const    attendance = this.props.attendance;
    let count = (this.state.sel_items.length) ? <span>({this.state.sel_items.length})</span> : null
    const qtyColor = 'red'
    return (<div>
      <Style scopeSelector='.table' rules={{
        backgroundColor: 'white',
        th: {
          color: 'white',
          backgroundColor: '#26A69A'
        }


      }}></Style>
    
      {this.state.sel_items.map(item => {
          console.log('item',item);
          return <p>{item.NAME}</p>
      })}
      <Table bsClass='table' responsive>
        <thead>
          <tr>
            <th></th>
            <th>
              S.No

            </th>
            <th>
              Date
            </th>
            <th>
              Item
            </th>
            <th>
              Quantity
            </th>
            <th>
              Delete all {count }
            </th>
          </tr>
        </thead>
        <tbody>
          {
            attendance.map((a, i) => {
              return <tr key={a.MEALDATE + i}>
                <td><form><input key={a.MEALDATE + i + 'checkbox'} id={a.MEALDATE + i + 'checkbox'} onChange={() => this.checkBoxChange(a ,i)} type='checkbox'></input></form></td>
                <td>{i + 1}</td>
                <td>{moment(a.MEALDATE).format('Do MMM YYYY')}</td>
                <td>{a.NAME}</td>
                <td>{a.QUANTITY}</td>
                <td onClick={()=>(this.props.paramhandle(item))}><Link to={`PurchaseOrder`}>Order</Link></td>
              </tr>
            })
          }
        </tbody>
      </Table>

    </div>);
  }
}
