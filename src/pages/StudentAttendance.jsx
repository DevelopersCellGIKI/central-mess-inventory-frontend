import React, { Component } from 'react';
import AttendanceList from '../components/AttendanceList.jsx'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../actions'
import Template from '../components/Template.jsx'
import { Col, Row, Form, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap'
import { Style } from 'radium'
import { List } from 'immutable'
class StudentAttendancePage extends Component {
    
  constructor(props) {
    super(props);
   
    this.state = {
      id: '',
      month: '',
      year: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setActiveItem = this.setActiveItem.bind(this)

  }
  getMonth() {
    const months=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
    return (
        months.map((item,i) => {
            return <option value={i+1} key={item}>{item}</option>
        })
    )
}
  setActiveItem(event) {
    const months=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];

    const val = event.target.value
    months.map((item,i) => {
        if (i+1 == val) {
           this.setState({
               month: i+1
               });
            return
        }
    })
 }  
  handleSubmit() {
   
    const { fetchStudentAttendance } = this.props.actions
    fetchStudentAttendance(this.state.id,this.state.month,this.state.year);

  }
  render() {

    
    return (<div className="headerSummaryPage">
      <Style scopeSelector=".headerSummaryPage" rules={{
        h3: {
          margin: 'initial'
        },
        input: {
          margin: 'initial'
        }
      }}>
      </Style>
      <Template>
        <Col sm={2}></Col>
        <Col sm={8}>
          <Row>
            <Col sm={7}>
              <h3>Student Attendance</h3>
            </Col>
            <Col sm={5}>
              <Form inline onSubmit={this.handleSubmit}>
                <FormGroup controlId="formInlineName">
                <ControlLabel>Student Regno</ControlLabel>

                <FormControl type="text" placeholder="Enter Student Regno" value={this.state.id} onChange={(event) => {
                this.setState({
                  id: event.target.value
                })
                }} />
                    
                <ControlLabel>Select Month</ControlLabel>
                <FormControl componentClass="select" placeholder="select" onChange={this.setActiveItem}>
                    {this.getMonth()}
                </FormControl>

                <ControlLabel>Select Year</ControlLabel>
                <FormControl type="number" placeholder="Enter Year" value={this.state.year} onChange={(event) => {
                    this.setState({
                      year: event.target.value
                    })
                  }} />
                </FormGroup>
                {' '}
                <Button type="submit">
                  <img src="icons/search.png" width="16px" height="16px"></img>
                </Button>
              </Form>
            </Col>
          </Row>
          <Row className="test">
            <Style scopeSelector=".test" rules={{
              margin: '1rem'
            }}>

            </Style>
            {
               this.props.data.get('isFetching') ? <div>Loading...</div> : 
               <AttendanceList attendance={this.props.data.get('attendance')} ></AttendanceList>

            }

          </Row>
        </Col>
        <Col sm={2}></Col>

      </Template>

    </div>);
  }
}
const mapStateToProps = state => (
  {
    data: state.studentAttendance,

  }
)
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Actions, dispatch)
})
export default connect(mapStateToProps, mapDispatchToProps)(StudentAttendancePage)
